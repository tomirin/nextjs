import React, { useState } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import { instance } from "../configs";
import Layout from "../layouts/Layout";

const Add = () => {
  const [user, setUser] = useState({ email: "", first_name: "", last_name: "", job: "" });

  const handleChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  //post data
  const handleClick = () => {
    instance
      .post("users", {
        email: user.email,
        first_name: user.first_name,
        last_name: user.last_name,
        job: user.job,
      })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <div>
      <Layout>
        <Row className="justify-content-center my-3">
          <Col md={6}>
            <Form.Label>First Name</Form.Label>
            <Form.Control name="first_name" type="text" onChange={handleChange} /> {/*tambahkan onchange*/}
          </Col>
        </Row>
        <Row className="justify-content-center mb-3">
          <Col md={6}>
            <Form.Label>Last Name</Form.Label>
            <Form.Control name="last_name" type="text" onChange={handleChange} /> {/*tambahkan onchange*/}
          </Col>
        </Row>
        <Row className="justify-content-center mb-3">
          <Col md={6}>
            <Form.Label>Email</Form.Label>
            <Form.Control name="email" type="email" onChange={handleChange} /> {/*tambahkan onchange*/}
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md={6}>
            <Form.Label>Job</Form.Label>
            <Form.Control name="job" type="text" onChange={handleChange} /> {/*tambahkan onchange*/}
          </Col>
        </Row>

        <Row className="text-center my-3">
          <Col>
            <a onClick={() => handleClick()} className="btn btn-primary">
              Add User
            </a>
          </Col>
        </Row>
      </Layout>
    </div>
  );
};

export default Add;
