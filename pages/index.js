import React, { useState } from "react";
import { Col, Row, Form } from "react-bootstrap";
import UserLists from "../components/user-lists";
import { host } from "../configs";
import Layout from "../layouts/Layout";
import Pagination from "../components/pagination";

export default function Home({ usersApi }) {
  // const data = users.data;
  const [users, setUsers] = useState(usersApi.data);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(3);

  const funcFilter = (e) => {
    const value = e.target.value;
    console.log(value);

    const filterData = users.filter((user) => {
      return user.first_name.search(value) != -1 || user.last_name.search(value) != -1;
    });

    if (value.length == 0 || value.trim() == "") {
      (async function () {
        const res = await host.get("users?");
        const { data } = res.data;
        setUsers(data);
      })();
    }
    setUsers(filterData);
  };

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = users.slice(indexOfFirstPost, indexOfLastPost);

  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  return (
    <Layout>
      <Row className="justify-content-md-center my-3">
        <Col md={6}>
          <Form.Control type="text" placeholder="Search Name..." onChange={funcFilter} />
        </Col>
      </Row>

      <Row md={3} className="text-center">
        <Col>
          <a href="/add" className="btn btn-primary">
            Add User
          </a>{" "}
        </Col>
      </Row>
      <UserLists users={currentPosts}></UserLists>
      <Pagination postsPerPage={postsPerPage} totalPosts={users.length} paginate={paginate} />
    </Layout>
  );
}

export async function getServerSideProps(context) {
  const res = await host.get("users?page=");
  const data = res.data;

  return {
    props: { usersApi: data }, // will be passed to the page component as props
  };
}
