import { useRouter } from "next/dist/client/router";

function UserDetail() {
  const router = useRouter();

  const idUser = router.query.uid;

  return (
    <div>
      {" "}
      <h1 className="text-center">Id : {idUser}</h1>
    </div>
  );
}

export default UserDetail;
