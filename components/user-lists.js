import React from "react";
import { Row } from "react-bootstrap";
import UserItem from "./user-item";

function UserLists({ users }) {
  return (
    <Row className="gy-3 my-3 justify-content-center" sm={4}>
      {users.map((user) => (
        <UserItem user={user} key={user.id}></UserItem>
      ))}
    </Row>
  );
}

export default UserLists;
