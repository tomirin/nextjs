import { useRouter } from "next/dist/client/router";
import React from "react";
import { Col, Card } from "react-bootstrap";

function UserItem({ user }) {
  const router = useRouter();

  const onLinkClick = (id) => {
    router.push(`UserDetail/${id}`);
  };

  return (
    <div>
      <Col>
        <Card onClick={() => onLinkClick(user.id)}>
          <Card.Img variant="top" src={user.avatar} />
          <Card.Body>
            <Card.Title>
              {user.first_name} {user.last_name}
            </Card.Title>
            <Card.Text>{user.email}</Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </div>
  );
}

export default UserItem;
