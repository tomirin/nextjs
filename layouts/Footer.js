import React, { Fragment } from "react";

function Footer() {
  const year = new Date().getFullYear();

  return (
    <footer className="stiky-footer">
      <div className="text-center my-4">
        <span>Copyright &copy; Your Website {year}</span>
      </div>
    </footer>
  );
}

export default Footer;
